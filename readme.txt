This is tested only on Windows for now.

1. You need to have PHP installed to run the script. Download and install PHP if you don't have it installed from here: http://windows.php.net/download
   a. Put the php folder inside your path variable:
   b. Start button -> right click computer click properties -> Advanced system settings -> Environment variables -> add ";C:\INSERT_PATH_HERE" without quotes to the end of your path Environment Variable
   c. Restart your computer.

2. If you used the version from the downloads section skip to step 7. If you have git installed and wish to clone the repository, continue to step 3.

== Cloning from git section ==

3. Clone the repo with git clone https://YOUR_USERNAME_HERE@bitbucket.org/iopq/automatic-algo-switcher-for-myriadcoin.git

4. Copy algos.ini.sample into algos.ini if you pulled from git.

5. Create shortcuts to your batch files for your miners inside the shortcuts folder named 'scrypt.lnk', 'groestl.lnk', etc. like in the algos.ini file, or change those paths to the paths of your existing shortcuts if you already mine Myriad. You can simply move your existing shortcuts to the shortcuts folder and change their names to 'groestl', 'skein', 'scrypt', 'qubit'.

6. Make sure you have --api-listen --api-allow W:127.0.0.1 in them so that they can be killed by the script to switch to a diff algo.

== Both ==

7. Click on run.bat; you're supposed to get two windows, the switcher itself and the miner. By using the algos.ini file you can control which algorithms are executed. If enabled is set to false, that algorithm doesn't run. Check every algorithm (by disabling everything else) and note the 'avg' speed you get in the miner.

8. Adjust your hashing speeds based on your individual set-up. You edit the hashing speed in algos.ini by putting  for example 830000 for 830kh/s, 15200000 for 15.2mh/s, etc. in the correct algorithm sections. The default speeds are based on my stock clock R9 290. Adjust based on your own rig to get correct hopping behavior.

9. Make sure your thread-concurrency is not set too high for your graphics card. The default value is for R9 290. For 5850 the best value seems to be 8191. If you get an error inside the miner that says "decrease TC" you should do so.

== Download option ==

10. If you used the download option, you need to change the .bat files inside the miner folder to point to your own credentials in pools, otherwise you'll be working for me (not that I mind). 
